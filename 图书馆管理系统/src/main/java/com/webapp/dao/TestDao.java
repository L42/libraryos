package com.webapp.dao;

import com.webapp.model.Test;

import javax.xml.registry.infomodel.User;


public interface TestDao {
    public Test selectUser(long id);
}
