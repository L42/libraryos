package com.webapp.service;

import com.webapp.model.Test;

public interface TestService {
    public Test selectUser(long id);
}
