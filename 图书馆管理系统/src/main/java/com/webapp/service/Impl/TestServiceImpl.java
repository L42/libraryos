package com.webapp.service.Impl;

import com.webapp.dao.TestDao;
import com.webapp.model.Test;
import com.webapp.service.TestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("testService")
public class TestServiceImpl implements TestService {
    @Resource
    private TestDao testDao;
    public Test selectUser(long id){
         return this.testDao.selectUser(id);
    }
}
