package com.webapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webapp.model.Test;
import com.webapp.service.TestService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller

public class test {
    @Resource
    private TestService testService;
    @RequestMapping("/showUser.do")
    public String selectUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Test test1 = (Test) testService.selectUser(1);
        System.out.println(test1);
        System.out.println("55555555555555555555555555555555555555555555555555555");
        return "seconds";
    }
}
